package preparing.impl.Kyiv;

import pizzaComponents.Dough;
import pizzaComponents.Sauce;
import pizzaComponents.Toppings;
import preparing.AbstractPreparePizza;

public class PreparePizzaClamKyiv extends AbstractPreparePizza {
    private Dough dough = Dough.thin;
    private Sauce sauce = Sauce.Pesto;
    private Toppings[] toppings = {Toppings.ham, Toppings.tomato, Toppings.mozzarella};


    public void takeDough() {
        super.takeDough(dough);
    }

    public void addSauce() {
        super.addSauce(sauce);
    }

    public void addToppings() {
        super.addToppings(toppings);
    }

}
