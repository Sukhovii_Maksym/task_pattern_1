import pizzas.AbstractBakery;
import pizzas.PizzaType;
import pizzas.impl.DniproBakeryPizza;
import pizzas.impl.KyivBakeryPizza;
import pizzas.impl.LvivBakeryPizza;

public class Application {
    public static void main(String[] args) {
        AbstractBakery bakeryKyiv = new KyivBakeryPizza();
        bakeryKyiv.createPizza(PizzaType.PEPPERONI);

        AbstractBakery bakeryLviv = new LvivBakeryPizza();
        bakeryLviv.createPizza(PizzaType.PEPPERONI);

        AbstractBakery bakeryDnipro = new DniproBakeryPizza();
        bakeryDnipro.createPizza(PizzaType.CHEESE);

    }
}
