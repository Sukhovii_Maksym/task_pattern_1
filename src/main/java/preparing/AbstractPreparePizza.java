package preparing;

import pizzaComponents.Dough;
import pizzaComponents.Sauce;
import pizzaComponents.Toppings;

public abstract class AbstractPreparePizza implements PreparePizza {
    public void takeDough(Dough dough) {
        System.out.println("Take " + dough + " dough");
    }

    public void addSauce(Sauce sauce) {
        System.out.println("Add " + sauce);
    }

    public void addToppings(Toppings... toppings) {
        System.out.println("Add toppings: ");
        for (Toppings topping : toppings) {
            System.out.print(topping + "; ");
        }
        System.out.println("");
    }


}
