package pizzas;

public abstract class AbstractBakery implements Pizza {
    public abstract void createPizza(PizzaType pizzaType);

}
