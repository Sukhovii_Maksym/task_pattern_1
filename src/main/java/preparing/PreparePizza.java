package preparing;

public interface PreparePizza {
    void takeDough();

    void addSauce();

    void addToppings();


}
