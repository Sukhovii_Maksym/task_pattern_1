package preparing.impl.Kyiv;

import pizzaComponents.Dough;
import pizzaComponents.Sauce;
import pizzaComponents.Toppings;
import preparing.AbstractPreparePizza;

public class PreparePizzaVeggieKyiv extends AbstractPreparePizza {
    private Dough dough = Dough.thick;
    private Sauce sauce = Sauce.Marinara;
    private Toppings[] toppings = {Toppings.tomato, Toppings.olives, Toppings.onion, Toppings.pepper};


    public void takeDough() {
        super.takeDough(dough);
    }

    public void addSauce() {
        super.addSauce(sauce);
    }

    public void addToppings() {
        super.addToppings(toppings);
    }

}
