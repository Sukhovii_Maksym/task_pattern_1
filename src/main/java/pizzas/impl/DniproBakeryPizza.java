package pizzas.impl;

import pizzas.AbstractBakery;
import pizzas.Pizza;
import pizzas.PizzaType;
import preparing.PreparePizza;
import preparing.impl.Dnipro.PreparePizzaCheeseDnipro;
import preparing.impl.Dnipro.PreparePizzaClamDnipro;
import preparing.impl.Dnipro.PreparePizzaPepperoniDnipro;
import preparing.impl.Dnipro.PreparePizzaVeggieDnipro;

public class DniproBakeryPizza extends AbstractBakery implements Pizza {
    PreparePizza preparePizza = null;

    public void createPizza(PizzaType pizzaType) {
        if (pizzaType == PizzaType.CHEESE) {
            preparePizza = new PreparePizzaCheeseDnipro();
        }
        if (pizzaType == PizzaType.CLAM) {
            preparePizza = new PreparePizzaClamDnipro();
        }
        if (pizzaType == PizzaType.VEGGIE) {
            preparePizza = new PreparePizzaVeggieDnipro();
        }
        if (pizzaType == PizzaType.PEPPERONI) {
            preparePizza = new PreparePizzaPepperoniDnipro();
        }
        prepare();
        bake();
        cut();
        box();
    }

    public void prepare() {
        preparePizza.takeDough();
        preparePizza.addSauce();
        preparePizza.addToppings();
    }

    public void bake() {
        System.out.println("Bake in Dnipro Baker!!!");
    }

    public void cut() {
        System.out.println("Cuted by Dnipro masters");
    }

    public void box() {
        System.out.println("Boxed in Dnipro ecobox");
    }


}
