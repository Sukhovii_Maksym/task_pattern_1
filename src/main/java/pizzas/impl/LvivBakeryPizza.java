package pizzas.impl;

import pizzas.AbstractBakery;
import pizzas.Pizza;
import pizzas.PizzaType;
import preparing.PreparePizza;
import preparing.impl.Lviv.PreparePizzaCheeseLviv;
import preparing.impl.Lviv.PreparePizzaClamLviv;
import preparing.impl.Lviv.PreparePizzaPepperoniLviv;
import preparing.impl.Lviv.PreparePizzaVeggieLviv;

public class LvivBakeryPizza extends AbstractBakery implements Pizza {
    PreparePizza preparePizza = null;

    public void createPizza(PizzaType pizzaType) {
        if (pizzaType == PizzaType.CHEESE) {
            preparePizza = new PreparePizzaCheeseLviv();
        }
        if (pizzaType == PizzaType.CLAM) {
            preparePizza = new PreparePizzaClamLviv();
        }
        if (pizzaType == PizzaType.VEGGIE) {
            preparePizza = new PreparePizzaVeggieLviv();
        }
        if (pizzaType == PizzaType.PEPPERONI) {
            preparePizza = new PreparePizzaPepperoniLviv();
        }
        prepare();
        bake();
        cut();
        box();
    }

    public void prepare() {
        preparePizza.takeDough();
        preparePizza.addSauce();
        preparePizza.addToppings();
    }

    public void bake() {
        System.out.println("Bake in Lviv Baker!!!");
    }

    public void cut() {
        System.out.println("Cuted by Lviv masters");
    }

    public void box() {
        System.out.println("Boxed in Lviv ecobox");
    }


}
