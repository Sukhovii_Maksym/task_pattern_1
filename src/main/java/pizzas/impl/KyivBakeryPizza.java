package pizzas.impl;

import pizzas.AbstractBakery;
import pizzas.Pizza;
import pizzas.PizzaType;
import preparing.PreparePizza;
import preparing.impl.Kyiv.PreparePizzaCheeseKyiv;
import preparing.impl.Kyiv.PreparePizzaClamKyiv;
import preparing.impl.Kyiv.PreparePizzaPepperoniKyiv;
import preparing.impl.Kyiv.PreparePizzaVeggieKyiv;

public class KyivBakeryPizza extends AbstractBakery implements Pizza {
    PreparePizza preparePizza = null;

    public void createPizza(PizzaType pizzaType) {
        if (pizzaType == PizzaType.CHEESE) {
            preparePizza = new PreparePizzaCheeseKyiv();
        }
        if (pizzaType == PizzaType.CLAM) {
            preparePizza = new PreparePizzaClamKyiv();
        }
        if (pizzaType == PizzaType.VEGGIE) {
            preparePizza = new PreparePizzaVeggieKyiv();
        }
        if (pizzaType == PizzaType.PEPPERONI) {
            preparePizza = new PreparePizzaPepperoniKyiv();
        }
        prepare();
        bake();
        cut();
        box();
    }

    public void prepare() {
        preparePizza.takeDough();
        preparePizza.addSauce();
        preparePizza.addToppings();
    }

    public void bake() {
        System.out.println("Bake in Kyiv Baker!!!");
    }

    public void cut() {
        System.out.println("Cuted by Kyiv masters");
    }

    public void box() {
        System.out.println("Boxed in Kyiv ecobox");
    }


}
