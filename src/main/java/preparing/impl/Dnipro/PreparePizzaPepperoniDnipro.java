package preparing.impl.Dnipro;

import pizzaComponents.Dough;
import pizzaComponents.Sauce;
import pizzaComponents.Toppings;
import preparing.AbstractPreparePizza;

public class PreparePizzaPepperoniDnipro extends AbstractPreparePizza {
    private Dough dough = Dough.thick;
    private Sauce sauce = Sauce.Marinara;
    private Toppings[] toppings = {Toppings.parmigiana, Toppings.olives, Toppings.salami, Toppings.pepper};


    public void takeDough() {
        super.takeDough(dough);
    }

    public void addSauce() {
        super.addSauce(sauce);
    }

    public void addToppings() {
        super.addToppings(toppings);
    }

}
