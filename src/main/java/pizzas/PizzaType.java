package pizzas;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}
