package pizzaComponents;

public enum Sauce {
    Marinara, Tomato, Pesto
}
